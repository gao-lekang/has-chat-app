const APIURL = "http://localhost:8090/";
const SOCKETAPI = "ws://localhost:8090/";
// const APIURL = "https://howcode.online";
// const SOCKETAPI = "wss://howcode.online";

const SERVICETOOLBOX = [{
		name: "照片",
		icon: "icon-zhaopian",
	},
	{
		name: "视频",
		icon: "icon-shipin",
	},
	{
		name: "录音",
		icon: "icon-luyin",
	},
];

module.exports = {
	APIURL,
	SOCKETAPI,
	SERVICETOOLBOX
}
